## General
Este proyecto(netamente de estudio) está basado en twitter, con un API REST en 
NodeJS versión 8.11.3, autenticacion mediante JWT, ExpressJS (manejo de rutas), 
MongoDB (Base de datos), Angular6 (frontend), Ajax (peticiones asíncronas al 
servidor), Bootstrap, HTML5 y CSS3.

## Componentes
Registro e inicio se sesión.
Edición de usuario.
Sistema para seguir y dejar de seguir un usuario.
Hacer publicaciones (con imagenes si se requiere).
Timeline donde se ven las publicaciones de las personas que seguimos.
Ver perfiles de usuario.
Mensajería privada.

## Aclaración
Este proyecto es solo de estudio sin fines de lucro, cualquier retrospectiva 
será bienvenida.
