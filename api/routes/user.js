'use strict'

var express = require('express');
var UserController = require('../controllers/user');

var api = express.Router();//El metodo Router() sirve para traer los metodos Get, Post, Put, Delete
var md_auth = require('../middlewares/authenticated')
var multipart = require('connect-multiparty');//Traemos la libreria multiparty
var md_upload = multipart({uploadDir: './uploads/users'})  //Creamos un middleware con multiparty para asignarle le ruta donde se guardara la imagen

api.get('/home', md_auth.ensureAuth, UserController.home);
api.post('/register', UserController.saveUser);
api.post('/login', UserController.login);
api.get('/user/:id', md_auth.ensureAuth, UserController.getUser); //Para pasar un parametro por url es: /:parametro
api.get('/users/:page?', md_auth.ensureAuth, UserController.getUsers);// El ? significa que el parametro es opcional
api.get('/counters/:id?', md_auth.ensureAuth, UserController.getCounters);
api.put('/update-user/:id', md_auth.ensureAuth, UserController.updateUser);
api.post('/upload-avatar/:id', [md_auth.ensureAuth, md_upload], UserController.uploadImage);//Cuando queremos pasar dos middlewares se pasa dentro de un array []
api.get('/get-avatar/:imageFile', UserController.getImageFile); //Obtener imagen

module.exports = api;