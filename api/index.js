'use strict'

//Conexión a la base de datos mongoDB
var mongoose = require('mongoose');
//Requerimos el archivo app.js que es donde tenemos express
var app = require('./app');
var port = 3000;
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/twitter'
        ).then(()=>{
             console.log('Conexion a la base de datos completada.')
             //Creamos Servidor
             app.listen(port, ()=>{//escuchamos el puerto en el que se va correr el app
                console.log('Servidor corriendo en http://localhost:'+port)
             });
            }
        ).catch(err => console.log(err));