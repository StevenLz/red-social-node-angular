'use strict'

var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var User = require('../models/user');
var Follow = require('../models/follow');
var Message = require('../models/message');

//FUNCION PARA ENVIAR MENSAJE
function saveMessage(req, res){
    var params = req.body;

    //Comprobamos si hay texto en el mensaje y comprobamos si hay un receptor
    if(!params.text || !params.receiver) return res.status(200).send({message:'Envia los datos necesarios'})
    //Primero se crea el objeto del modelo
    var message = new Message();
    //Luego setteamos las propiedades del modelo
    message.text = params.text;
    message.viewed = 'false';
    message.created_at = moment().unix();
    message.emitter = req.user.sub;
    message.receiver = params.receiver;

    message.save((err, messageStored)=>{
        if(err) return res.status(500).send({message: 'Error en la peticion'});

        if(!messageStored) return res.status(404).send({message: 'No se pudo enviar el mensaje'});

        return res.status(200).send({messageStored})
    })
}

//FUNCION PARA OBETENER MENSAJES RECIBIDOS
function getReceivedMessages(req, res){
    var userId = req.user.sub;
    var page = 1;

    if(req.params.page){
        page = req.params.page
    }

    var itemsPerPage = 4;
    //En el .populate() podemos pasar un segundo parametro que sirve para que la consulta solo me traiga los datos que necesito
    //Lo organizamos por fecha de creacion con .sort()
    Message.find({receiver : userId}).populate('emitter', 'name surname nick image _id').sort('-created_at').paginate(page, itemsPerPage, (err, messages, total)=>{
        if(err) return res.status(500).send({message: 'Error en la peticion'});

        if(!messages) return res.status(404).send({message: 'No hay mensajes'});

        return res.status(200).send({
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            messages
        })
    })
}

//FUNCION PARA OBETENER MENSAJES ENVIADOS
function getEmmitMessages(req, res){
    var userId = req.user.sub;
    var page = 1;

    if(req.params.page){
        page = req.params.page
    }

    var itemsPerPage = 4;
    //En el .populate() podemos pasar un segundo parametro que sirve para que la consulta solo me traiga los datos que necesitemos
    Message.find({emitter : userId}).populate('emitter receiver', 'name surname nick image _id').sort('-created_at').paginate(page, itemsPerPage, (err, messages, total)=>{
        if(err) return res.status(500).send({message: 'Error en la peticion'});

        if(!messages) return res.status(404).send({message: 'No hay mensajes'});

        return res.status(200).send({
            total: total,
            pages: Math.ceil(total/itemsPerPage),
            messages
        })
    })
}

//FUNCION PARA OBTENER MENSAJES SIN LEER
function getUnviewedMessages(req, res){
    var userId = req.user.sub;
    //Hacemos el conteo donde el receptor sea el usuario logeado y la propiedad viewed sea false (osea no leido)
    Message.count({receiver: userId, viewed: 'false'}).exec((err, count)=>{
        if(err) return res.status(500).send({message: 'Error en la peticion'});

        return res.status(200).send({
            'unviewed': count
        }) 
    })
}

//FUNCION PARA MARCAR TODOS LOS MENSAJES COMO LEIDOS
function setViewedMessages(req, res){
    var userId = req.user.sub;
    //Hacemos un update primero pasando las condiciones en este caso es: receiver: user logueado, y que el mensaje no haya sido visto viewed: false
    //Como segundo parametro pasamos el campo a editar en este caso es viewed por lo tanto lo pasamos asi: {viewed: 'true'}
    //Como tercer parametro pasamos si se quiere editar multiples colecciones, en este caso si entonces pasamos: {'multi': true}
    Message.update({receiver: userId, viewed: 'false'}, {viewed: 'true'}, {'multi': true}, (err, messageUpdated)=>{
        if(err) return res.status(500).send({message: 'Error en la peticion'});

        return res.status(200).send({ 
            messages : messageUpdated 
        })
    })
}
module.exports = {
    saveMessage,
    getReceivedMessages,
    getEmmitMessages,
    getUnviewedMessages,
    setViewedMessages
}