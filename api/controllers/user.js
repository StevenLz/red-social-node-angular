'use strict'

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');//Requerimos el modelo de usuarios
var Publication = require('../models/publication')
var Follow = require('../models/follow');//Requerimos el modelo de follows
var jwt = require('../services/jwt');//Requerimos el servicio del token para el login
var fs = require('fs');
var path = require('path');
var mongoosePaginate = require('mongoose-pagination')

function home(req, res) {
    res.status(200).send({
        message: 'Bienvenido a Twitter'
    });
}

//REGISTRO DE USUARIO
function saveUser(req, res) {
    var params = req.body; // Requerimos los parametros que vienen por el body de la peticion
    var user = new User(); //Instanciamos el objeto User, que es donde tenemos el modelo

    if (params.name && params.surname && params.nick && params.email && params.password) { //Verificamos que los campos obligatios esten completos
        user.name = params.name;
        user.surname = params.surname;
        user.nick = params.nick;
        user.email = params.email;
        user.role = 'ROLE_USER';
        user.image = null;

        //Controlar usuarios duplicados
        User.find({
            $or: [
                { email: user.email.toLowerCase() },
                { nick: user.nick.toLowerCase() }
            ]
        }).exec((err, users) => {
            if (err) return res.status(500).send({ message: 'Error en la peticion de usuario' });

            if (users && users.length >= 1) {
                return res.status(200).send({ message: 'El email o nick ya esta registrado!' })
            } else {
                //Cifrar password y guardar datos de usuario
                bcrypt.hash(params.password, null, null, (err, hash) => { //Encriptamos la password
                    user.password = hash;

                    user.save((err, UserStored) => {
                        if (err) return res.status(500).send({ message: 'Error al guardar usuario' })
                        if (UserStored) {
                            res.status(200).send({ user: UserStored })
                        } else {
                            res.status(404).send({ message: 'No se ha registrado el usuario' })
                        }
                    })
                })
            }
        });
    } else {
        res.status(200).send({ message: 'Envia todos los campos necesarios.' })
    }
}

//LOGIN
function login(req, res) {
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({ email: email }, (err, user) => { //Se compara solo si el email que el usuario esta ingresando existe en la base de datos
        if (err) return res.status(500).send({ message: 'Error en el inicio de sesion' })

        if (user) {
            bcrypt.compare(password, user.password, (err, check) => { //Si el email existe, se procede a comparar la password ingresada, se compara con la libreria bcrypt(encriptada)
                if (check) {
                    if (params.gettoken) { //Establecemos un parametro gettoken
                        //Generar y devolver token
                        return res.status(200).send({
                            token: jwt.createToken(user)//Traemos la funcion de crear token del service jwt que creamos
                        });
                    } else {
                        //Devolvemos los datos del usuario
                        user.password = undefined; //Esto se usa para eliminar una propiedad y no se muestre en el status.send, con esto la contraseña no saldra en el JSON
                        return res.status(200).send({ user })
                    }
                } else {
                    return res.status(404).send({ message: 'La contraseña no coincide' })
                }
            })
        } else {
            return res.status(404).send({ message: 'No se encontro el usuario' })
        }
    });
}

//Devolver usuario por id en la URL
function getUser(req, res) {
    var userId = req.params.id; //El req.params es para tomar datos de la url

    User.findById(userId, (err, user) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!user) return res.status(404).send({ message: 'No se encontro el usuario' });

        followThisUser(req.user.sub, userId).then((value) => {
            user.password = undefined;
            return res.status(200).send({
                user,
                following: value.following,
                followed: value.followed
            });
        })
    })
}
//Creamos una funcion async await lo que haremos es crear las dos consultas para saber si el usuario,
//Al cual accedemos por url nos sigue y tambien saber si nosotros lo seguimos
//Las funciones async await nos sirve para no hacer multiples callback en una misma funcion
async function followThisUser(identity_user_id, user_id) {
    try {
        //Variable para consultar si seguimos al usuario de la url
        //Lo que hacemos es darle un await a la consulta para esperar que nos genere el dato para poderlo retornar.
        //Lo que hacemos con await es volver el codigo sincrono, esperaremos a que se ejecute una linea para pasar a la otra y asi poder tener un valor antes de que se ejecute la segunda
        //Si no lo hacemos asi pasaria: si la consulta se demora 1ms mas que retornar el dato, primero retornara la variable vacia.
        var following = await Follow.findOne({ "user": identity_user_id, "followed": user_id }).exec().then((following) => {
            return following;
        }).catch((err) => {
            return handleError(err);
        })
        //Variable para consultar si el usuario de la url nos sigue a nosotros
        var followed = await Follow.findOne({ "user": user_id, "followed": identity_user_id }).exec().then((followed) => {
            return followed;
        }).catch((err) => {
            if (err) return handleError(err);
        });
        //Devolvemos los parametros con los resultados de ambas consultas
        //Generamos una promesa
        return {
            following: following,
            followed: followed
        }
    } catch (e) {
        console.log(e);
    }

}

//Devolver listado de usuarios paginado
function getUsers(req, res) {
    var identity_user_id = req.user.sub;//Traemos el id del usuario del token (usuario logeado)

    var page = 1;//Establecemos la pagina por defecto inicial (1)
    if (req.params.page) {//Verificamos si hay una pagina en la URL
        page = req.params.page; //Si hay una pagina en la URL la traemos en la variable page
    }

    var itemsPerPage = 5; //Establecemos la cantidad de items que habrá por pagina

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {//Buscamos los usuarios y los organizamos por ID(.sort) - Paginamos los datos(.paginate) - obtenemos el total de items encontrados(total)
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!users) return res.status(404).send({ message: 'No hay usuarios disponibles' });

        followUserIds(identity_user_id).then((value) => {
            return res.status(200).send({
                users, //Traemos los datos de los usuarios encontrados
                users_following: value.following,
                users_follow_me: value.followed,
                total,  //Traemos la cantidad de items encontrados
                pages: Math.ceil(total / itemsPerPage) //Establecemos el numero de paginas dividiendo el total de datos encontrados por los items por pagina
            });
        })
    });
}

//Este codigo se puede hacer en un archivo aparte para reutilizarlo como el JWT en services
async function followUserIds(user_id) {
    try {
        //El .select() nos sirve para borrar campos de la consulta y obtener solo los que necesitamos (en este caso : _id, __v, user)
        //Que usuarios seguimos
        var following = await Follow.find({ "user": user_id }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec().then((follows) => {
            //Creamos el objeto vacio para luego rellenarlo con el forEach
            var followingArr = [];
            //Recorremos la consulta con un for
            follows.forEach((follow) => {
                //Con cada iteracion del for hacemos push a los datos
                followingArr.push(follow.followed)
            })
            return followingArr;
        }).catch((err) => {
            return handleError(err);
        })
        //Que usuarios nos siguen
        var followed = await Follow.find({ "followed": user_id }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec().then((follows) => {
            //Creamos el objeto vacio para luego rellenarlo con el forEach
            var followingArr = [];
            //Recorremos la consulta con un for
            follows.forEach((follow) => {
                //Con cada iteracion del for hacemos push a los datos
                followingArr.push(follow.user)
            })
            return followingArr;
        }).catch((err) => {
            return handleError(err);
        })
        return {
            following: following,
            followed: followed
        }
    } catch (e) {
        console.log(e)
    }
}
//CONTADORES SE SEGUIDORES Y SEGUIDOS
function getCounters(req, res) {
    //Por defecto tiene el id del usuario logueado
    var userId = req.user.sub;
    //Si viene por id por url lo establecemos como parametro de la consulta
    if (req.params.id) {
        userId = req.params.id;
    }

    getCountFollow(userId).then((value) => {
        return res.status(200).send(value);
    })
}
//Creamos una funcion asyn await para realizar las dos consultas del contador de following y followed
async function getCountFollow(user_id) {
    try {
        var following = await Follow.count({ 'user': user_id }).exec().then((count) => {
            return count;
        }).catch((err) => {
            return handleError(err);
        })
        var followed = await Follow.count({ 'followed': user_id }).exec().then((count) => {
            return count;
        }).catch((err) => {
            return handleError(err);
        })


        var publications = await Publication.count({ 'user': user_id }).exec().then((count) => {
            return count;
        }).catch((err) => {
            return handleError(err);
        })
        return {
            following: following,
            followed: followed,
            publications: publications
        }
    } catch (e) {
        console.log(e);
    }
}

//Funcion para editar usuario
function updateUser(req, res) {
    var userId = req.params.id;
    var update = req.body;//Traemos los datos a editar

    delete update.password;//Eliminamos la propiedad password ya que no la queremos editar aqui

    if (userId != req.user.sub) { //Validamos que el ID del logeado sea el mismo al ID a editar
        return res.status(500).send({ message: 'No tienes permiso para editar este usuario' });
    }
    //VERIFICACION SI AL ACTUALIZAR SE ENCUENTRA UN EMAIL O NICK IGUAL.
    User.find({
        $or: [
            { email: update.email.toLowerCase() },
            { nick: update.nick.toLowerCase() }
        ]
    }).exec((err, users) => {
        console.log(users);
        var user_isset = false;
        users.forEach((user) => {
            if (user && user._id != userId) user_isset = true; //Aqui si encuentra un registro igual, establece la variable user_isset en true, esto se hace para que se envie solo una cabecera
        })
        //Sacamos el envio de la cabecera del forEach para evitar el error: "Cant set headers after they are sent"
        if(user_isset) return res.status(404).send({ message: 'Los datos ya estan en uso.' });//Miramos si en el forEach nos da un true

        //Buscamos la coleccion por ID, y pasamos como segundo parametro el update que es el que contiene lso datos
        User.findByIdAndUpdate(userId, update, { new: true }, (err, userUpdated) => { // {new:true} sirve para ver los datos nuevos actualizados y no los viejos
            if (err) return res.status(500).send({ message: 'Error en la peticion' });

            if (!userUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' })

            return res.status(200).send({ user: userUpdated });
        });
    });
}

// Subir imagen de avatar
function uploadImage(req, res) {
    var userId = req.params.id;

    if (req.files) {//Comprobamos si hay archivos en el body

        var file_path = req.files.image.path; //Traemos los archivos y sacamos el path del campo image
        var file_split = file_path.split('\\') //Cortamos el path tomando como referencia el \ para sacar el nombre del archivo
        var file_name = file_split[2]; //Ya que cortamos el path, queda como un array y podemos acceder al nombre de la imagen
        var ext_split = file_name.split('\.'); //De la misma manera que cortamos el path, cortamos el nombre del archivo tomando como referencia el . para sacar la extencion del archivo
        var file_ext = ext_split[1]; //Ya que cortamos el nombre del archivo, queda un array y podemos acceder a la extencion de la imagen

        if (userId != req.user.sub) { //Validamos que el ID del logeado sea el mismo al ID a editar
            //Debemos retornar la funcion que creamos
            return removeUploadFiles(res, file_path, 'No tienes permiso para actualizar este usuario'); //Traemos la funcion que creamos para remover archivos cuando hay error
        }

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg') {//Primero verificamos que la extencion del archivo sea una imagen
            User.findByIdAndUpdate(userId, { image: file_name }, { new: true }, (err, userUpdated) => { //Ya que queremos solo actualizar la imagen, la pasamos por un parametro {image: file_name}
                if (err) return res.status(500).send({ message: 'Error en la peticion' });

                if (!userUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' })

                return res.status(200).send({ user: userUpdated });
            })
        } else {
            //Debemos retornar la funcion que creamos
            return removeUploadFiles(res, file_path, 'Extension no valida'); //Traemos la funcion que creamos para remover archivos cuando hay error
        }

    } else {
        return res.status(200).send({ message: 'No se han subido imagenes' });
    }
}

//ELIMINAR ARCHIVOS
function removeUploadFiles(res, file_path, message) { //Creamos una funcion para remover archivo en caso de error y poder usarla de manera mas practica dentro de otras funciones
    fs.unlink(file_path, (err) => {// fs.unlink sirve para borrar un archivo local
        return res.status(200).send({ message: message });
    })
}
//OBTENER IMAGEN
function getImageFile(req, res) {
    var image_file = req.params.imageFile;//Traemos el nombre de la imagen por ruta
    var path_file = './uploads/users/' + image_file; //Concatenamos la ruta del fichero local con el nombre la imagen de la ruta

    fs.exists(path_file, (exists) => { //Miramos si existe la imagen
        if (exists) {//Si existe la imagen
            res.sendFile(path.resolve(path_file));//Enviamos la imagen en caso de que exista con sendFile
        } else {
            res.status(200).send({ message: 'No existe la imagen' });
        }
    })
}

module.exports = {
    home,
    saveUser,
    login,
    getUser,
    getUsers,
    getCounters,
    updateUser,
    uploadImage,
    getImageFile
}