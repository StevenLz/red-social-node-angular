'user strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Publication = require('../models/publication');
var User = require('../models/user');
var Follow = require('../models/follow');

function createPublication(req, res) {
    var params = req.body;

    var publication = new Publication();

    if (!params.text) return res.status(200).send({ message: 'Debes enviar un texto' });

    publication.text = params.text;
    publication.file = 'null';
    publication.user = req.user.sub;
    publication.created_at = moment().unix();

    publication.save((err, publicationStored) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!publicationStored) return res.status(404).send({ message: 'La publicacion no ha sido guardada' });

        return res.status(200).send({ publication: publicationStored });
    })

}
//OBTENER PUBLICACIONES
function getPublications(req, res) {
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 4;
    //Buscamos los usuarios que seguimos
    Follow.find({ user: req.user.sub }).populate('followed').exec((err, follows) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        var followsArr = [];
        //Ponemos los usuarios que seguimos dentro de un objeto
        follows.forEach((follow) => {
            followsArr.push(follow.followed);
        });
        followsArr.push(req.user.sub);//Pusheamos al array nuestro id para poder hacer la consulta y nos muestre tambien las publicacion que tienen nuestro id

        // {"$in": followsArr} nos sirve para hacer la consulta con base a un objeto que tengamos
        //El buscara todas las publicaciones que tengan como id los que tenemos en el objeto followsArr
        //Luego con .sort organizaremos las publicaciones desde la mas nueva a la mas antigua
        //Buscamos las publicaciones de los usuarios que seguimos
        Publication.find({ user: { "$in": followsArr } }).sort('-created_at').populate('user').paginate(page, itemsPerPage, (err, publications, total) => {
            if (err) return res.status(500).send({ message: 'Error en la peticion' });

            if (!publications) return res.status(404).send({ message: 'No hay publicaciones' })

            return res.status(200).send({
                total,
                pages: Math.ceil(total / itemsPerPage),
                page: page,
                items_per_page: itemsPerPage,
                publications
            });
        });
    });
}
//PUBLICACIONES DE UN USUARIO EN ESPECIFICO
function getPublicationsUser(req, res) {
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    }

    var user = req.user.sub;

    if(req.params.user){
        user = req.params.user;
    }

    var itemsPerPage = 4;
    // {"$in": followsArr} nos sirve para hacer la consulta con base a un objeto que tengamos
    //El buscara todas las publicaciones que tengan como id los que tenemos en el objeto followsArr
    //Luego con .sort organizaremos las publicaciones desde la mas nueva a la mas antigua
    //Buscamos las publicaciones de los usuarios que seguimos
    Publication.find({ user: user }).sort('-created_at').populate('user').paginate(page, itemsPerPage, (err, publications, total) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!publications) return res.status(404).send({ message: 'No hay publicaciones' })

        return res.status(200).send({
            total,
            pages: Math.ceil(total / itemsPerPage),
            page: page,
            items_per_page: itemsPerPage,
            publications
        });
    });
}

function getPublication(req, res) {
    var publicationId = req.params.id;
    Publication.findById(publicationId, (err, publication) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!publication) return res.status(404).send({ message: 'La publicación no existe' });

        return res.status(200).send({ publication })
    })
}

function deletePublication(req, res) {
    var publicationId = req.params.id;
    //Buscamos la publicacion que nos llega por url y comprobamos si es del usuario logeado, si es asi se puede borrar
    Publication.findOne(({ 'user': req.user.sub, '_id': publicationId })).remove((err, publicationRemoved) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });
        //Cuando se va a remover genera un objeto con 2 parametros: n y ok, cuando sale en n > 0 significa que borro el objeto, si sale n:0 no borro nada
        //Por eso comprobamos si borro para enviar el mensaje indicado
        if (publicationRemoved.n > 0) {
            return res.status(200).send({ message: 'Publicación eliminada correctamente' })
        } else {
            return res.status(404).send({ message: 'No tienes permisos para borrar la publicación' });
        }

    })
}

// Subir imagen de avatar
function uploadImage(req, res) {
    var publicationId = req.params.id;

    if (req.files) {//Comprobamos si hay archivos en el body

        var file_path = req.files.image.path; //Traemos los archivos y sacamos el path del campo image
        var file_split = file_path.split('\\') //Cortamos el path tomando como referencia el \ para sacar el nombre del archivo
        var file_name = file_split[2]; //Ya que cortamos el path, queda como un array y podemos acceder al nombre de la imagen
        var ext_split = file_name.split('\.'); //De la misma manera que cortamos el path, cortamos el nombre del archivo tomando como referencia el . para sacar la extencion del archivo
        var file_ext = ext_split[1]; //Ya que cortamos el nombre del archivo, queda un array y podemos acceder a la extencion de la imagen

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg') {//Primero verificamos que la extencion del archivo sea una imagen
            //Comprobamos si el usuario logueado ese el mismo dueño de la publicacion
            Publication.findOne({ 'user': req.user.sub, '_id': publicationId }).exec((err, publication) => {
                //Si es el mismo usuario ejecutamos la actualizacion
                if (publication) {
                    Publication.findByIdAndUpdate(publicationId, { file: file_name }, { new: true }, (err, publicationUpdated) => { //Ya que queremos solo actualizar la imagen, la pasamos por un parametro {image: file_name}
                        if (err) return res.status(500).send({ message: 'Error en la peticion' });

                        if (!publicationUpdated) return res.status(404).send({ message: 'No se ha podido actualizar el usuario' })

                        return res.status(200).send({ publication: publicationUpdated });
                    })
                } else {
                    //Si es un usuario diferente removemos el archivo y enviamos un mensaje
                    return removeUploadFiles(res, file_path, 'No tienes permiso para actualizar esta publicacion');
                }
            })
        } else {
            //Debemos retornar la funcion que creamos
            return removeUploadFiles(res, file_path, 'Extension no valida'); //Traemos la funcion que creamos para remover archivos cuando hay error
        }

    } else {
        return res.status(200).send({ message: 'No se han subido imagenes' });
    }
}

//ELIMINAR ARCHIVOS
function removeUploadFiles(res, file_path, message) { //Creamos una funcion para remover archivo en caso de error y poder usarla de manera mas practica dentro de otras funciones
    fs.unlink(file_path, (err) => {// fs.unlink sirve para borrar un archivo local
        return res.status(200).send({ message: message });
    })
}
//OBTENER IMAGEN
function getImageFile(req, res) {
    var image_file = req.params.imageFile;//Traemos el nombre de la imagen por ruta
    var path_file = './uploads/publications/' + image_file; //Concatenamos la ruta del fichero local con el nombre la imagen de la ruta

    fs.exists(path_file, (exists) => { //Miramos si existe la imagen
        if (exists) {//Si existe la imagen
            res.sendFile(path.resolve(path_file));//Enviamos la imagen en caso de que exista con sendFile
        } else {
            res.status(200).send({ message: 'No existe la imagen' });
        }
    })
}
module.exports = {
    createPublication,
    getPublications,
    getPublication,
    deletePublication,
    uploadImage,
    getImageFile,
    getPublicationsUser
}