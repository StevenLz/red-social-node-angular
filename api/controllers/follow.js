'use strict'

var Follow = require('../models/follow');
var User = require('../models/user');

function saveFollow(req, res) {
    var params = req.body;
    var follow = new Follow();

    follow.user = req.user.sub;
    follow.followed = params.followed;

    follow.save((err, followStored) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!followStored) return res.status(404).send({ message: 'No se pudo seguir al usuario' });

        return res.status(200).send({ follow: followStored })
    })
}

function deleteFollow(req, res) {
    var userId = req.user.sub;
    var followed = req.params.id;

    Follow.find({ 'user': userId, 'followed': followed }).remove(err => { //Pasamos los parametros de la consulta que son: userId y followed, posterior a esto eliminamos la coleccion encontrada
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        return res.status(200).send({ message: 'Has dejado de seguir a este usuario' });
    })
}
//Obtener a quien sigo
function getFollowingUsers(req, res) {
    var userId = req.user.sub; //Si no viene id por la url mostramos los seguimientos del usuario logueado
    //Aqui miramos si vienen los dos parametros por la URL
    if (req.params.id && req.params.page) {//Damos prioridad al id que viene por la url, si tenemos un id en la url entonces:
        userId = req.params.id; //El valor de userId es el que viene por la url
    }

    var page = 1;//Establecemos la pagina principal en caso de que no venga ninguna por url
    if (req.params.page) { //En caso de que venga la pagina por url:
        page = req.params.page //Le damos el valor a la variable tomando el valor que viene por url
    } else {
        page = req.params.id;
    }

    var itemsPerPage = 4;

    //Con .populate lo que hacemos es traer el objeto completo(nick, name, surname, email, etc) al cual estoy siguiendo, tomando como referencia el campo followed 
    //Buscamos los usuarios que estamos siguiendo
    Follow.find({ user: userId }).populate({ path: 'followed' }).paginate(page, itemsPerPage, (err, follows, total) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!follows) return res.status(404).send({ message: 'No estas siguiendo a nadie' });
        followUserIds(req.user.sub).then((value) => {
            return res.status(200).send({
                total: total,
                pages: Math.ceil(total / itemsPerPage),
                follows,
                users_following: value.following,
                users_follow_me: value.followed
            })
        })
    })
}
//Este codigo se puede hacer en un archivo aparte para reutilizarlo como el JWT en services
async function followUserIds(user_id) {
    try {
        //El .select() nos sirve para borrar campos de la consulta y obtener solo los que necesitamos (en este caso : _id, __v, user)
        //Que usuarios seguimos
        var following = await Follow.find({ "user": user_id }).select({ '_id': 0, '__v': 0, 'user': 0 }).exec().then((follows) => {
            //Creamos el objeto vacio para luego rellenarlo con el forEach
            var followingArr = [];
            //Recorremos la consulta con un for
            follows.forEach((follow) => {
                //Con cada iteracion del for hacemos push a los datos
                followingArr.push(follow.followed)
            })
            return followingArr;
        }).catch((err) => {
            return handleError(err);
        })
        //Que usuarios nos siguen
        var followed = await Follow.find({ "followed": user_id }).select({ '_id': 0, '__v': 0, 'followed': 0 }).exec().then((follows) => {
            //Creamos el objeto vacio para luego rellenarlo con el forEach
            var followingArr = [];
            //Recorremos la consulta con un for
            follows.forEach((follow) => {
                //Con cada iteracion del for hacemos push a los datos
                followingArr.push(follow.user)
            })
            return followingArr;
        }).catch((err) => {
            return handleError(err);
        })
        return {
            following: following,
            followed: followed
        }
    } catch (e) {
        console.log(e)
    }
}

//Obtener quien me sigue
function getFollowedUsers(req, res) {
    var userId = req.user.sub; //Si no viene id por la url mostramos los seguimientos del usuario logueado
    //Aqui miramos si vienen los dos parametros por la URL
    if (req.params.id && req.params.page) {//Damos prioridad al id que viene por la url, si tenemos un id en la url entonces:
        userId = req.params.id; //El valor de userId es el que viene por la url
    }

    var page = 1;//Establecemos la pagina principal en caso de que no venga ninguna por url
    if (req.params.page) { //En caso de que venga la pagina por url:
        page = req.params.page //Le damos el valor a la variable tomando el valor que viene por url
    } else {
        page = req.params.id;
    }

    var itemsPerPage = 4;

    //Con .populate lo que hacemos es traer el objeto completo(nick, name, surname, email, etc) al cual estoy siguiendo, tomando como referencia el campo followed 
    //Buscamos los usuarios que estamos siguiendo
    Follow.find({ followed: userId }).populate('user').paginate(page, itemsPerPage, (err, follows, total) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!follows) return res.status(404).send({ message: 'No te sigue nadie' });

        followUserIds(req.user.sub).then((value) => {
            return res.status(200).send({
                total: total,
                pages: Math.ceil(total / itemsPerPage),
                follows,
                users_following: value.following,
                users_follow_me: value.followed
            })
        })
    })
}

//Obtener listados sin paginar de quien me sigue y a quien sigo
function getFollows(req, res) {
    var userId = req.user.sub;
    //Establecemos una variable find donde pasaremos la consulta si es lista de followed o following
    var find = Follow.find({ user: userId });//En este caso arrancamos con following como consulta por defecto
    //Si por url nos llega que es followed entonces cambiamos el valor de la variable a followed
    if (req.params.followed) {
        find = Follow.find({ followed: userId });
    }
    //Pasamos la variable find y la populamos con user y followed
    find.populate('user followed').exec((err, follows) => {
        if (err) return res.status(500).send({ message: 'Error en la peticion' })

        if (!follows) return res.status(404).send({ message: 'No se han encontrado usuarios' })

        return res.status(200).send({ follows });
    })
}

module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUsers,
    getFollows
}