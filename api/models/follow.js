'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FollowSchema = Schema({
    user: { type: Schema.ObjectId, ref:'User' },//Asi traemos una referencia de un id de otra coleccion.
    followed: { type: Schema.ObjectId, ref:'User' }//Asi traemos una referencia de un id de otra coleccion.
});

module.exports = mongoose.model('Follow', FollowSchema);