'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Indicamos la estructura de datos que tendra la entidad
var UserSchema = Schema({
    name: String,
    surname: String,
    nick: String,
    email: String,
    password: String,
    role: String,
    image: String,
});

module.exports = mongoose.model('User', UserSchema); // Le indicamos ('Nombre identidad', Indicamos el esquema(formato))
//Cuando indicamos 'User' mongoose lo pluraliza la palabra y la pone en minuscula queda asi: users, automaticamente