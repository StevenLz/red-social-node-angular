'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'proyecto_twitter_stevenlz';

exports.ensureAuth = function(req, res, next){
    if(!req.headers.authorization){ //Verificamos si no trae la cabecera de autorizacion
        return res.status(403).send({message : 'La peticion no tiene la cabecera de autenticacion'});
    }

    var token = req.headers.authorization.replace(/['"]+/g, ''); //Expresion regular para quitar las comillas simples y dobles
    
    try{
        var payload = jwt.decode(token, secret);//Decodificamos el token con la clave secreta

        if(payload.exp <= moment().unix()){ //Verificamos que la fecha de expiracion del token no sea menor que la fecha actual
            return res.status(401).send({message : 'El token ha expirado'})
        }
    }catch(ex){
        return res.status(404).send({message: 'El token no es valido'})
    }

    req.user = payload; //Con esto cargamos el payload en el req.user y cada que necesitemos datos del user logeado se trae de aqui
    
    next();//Saltamos a lo siguiente que va ejecutar node
}