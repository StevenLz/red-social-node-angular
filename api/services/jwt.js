'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'proyecto_twitter_stevenlz';//Se establece una variable con la clave secreta de cifrado del token

exports.createToken = function(user){
    var payload = {
        sub: user._id,
        name: user.name,
        surname: user.surname,
        nick: user.nick,
        email: user.email,
        role: user.role,
        image: user.image,
        iat: moment().unix(),//Fecha de creacion del token, creada con la fecha actual, en formato unix
        exp: moment().add(30, 'days').unix()//Fecha de expiracion del token, se toma el dia actual y se le agregan 30 dias, en formato unix
    };
    return jwt.encode(payload, secret); //Encriptamos el token, pasando los datos del usuario con la clave secreta
} 