import { Component, OnInit } from '@angular/core';//El OnInit es el metodo que se ejecuta apenas cargamos un componente

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
    public title:string;

    constructor(){
        this.title = 'Bienvenido a NODESocial';
    }

    ngOnInit(){
        console.log('Componente home');
    }
}

