import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    providers: [UserService]//Aqui cargamos los services que necesitamos para el componente
})

export class RegisterComponent implements OnInit{
    public title:string;
    //Creamos la variable user que contendra el modelo de usuario
    public user:User;
    public status:string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService //establecemos una variable privada donde tendra el servicio de usuario
    ){
        this.title = 'Registrate';
        //Le pasamos el valor a la variable user que sera el modelo User con todos sus campos vacios
        this.user = new User('',
            '',
            '',
            '',
            '',
            '',
            'ROLE_USER',
            ''
        );
    }

    ngOnInit(){
        console.log('Componente Register');  
    }

    onSubmit(form){
        //Traemos el metodo de register del servicio de usuario
        this._userService.register(this.user).subscribe(//Subscribe sirve para obtenemos lo que nos devuelve el API
            response => { //Respuesta del api
                if(response.user && response.user._id){ //Si la respuesta trae un objeto user y a tambien trae el id de ese usuario entonces:
                    //console.log(response.user); 
                    this.status = 'success'; //Entonces le pasamos a la variable status el valor de success, o sea que se guardo correctamente
                    form.reset();//Esto limpia el formulario, el form es el parametro que pasamos desde el html en el onSubmit
                }else{
                    this.status = 'error'; //En caso de que no traiga el objeto con el id establecemos a status con el valor error 
                }
            },
            error => { //Si tenemos un error lo imprimimos aqui
                console.log(<any>error);
                
            }
        );   
    }
}