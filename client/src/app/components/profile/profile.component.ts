import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Follow } from '../../models/follow';
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';
import { GLOBAL } from '../../services/global';
import { LoginComponent } from '../login/login.component';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    providers: [UserService, FollowService]
})

export class ProfileComponent implements OnInit {
    public title: string;
    public user: User;
    public status: string;
    public identity;
    public token;
    public stats;
    public url;
    public followed;
    public following;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _followService: FollowService
    ) {
        this.title = "Perfil";
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.url = GLOBAL.url;
        this.followed = false;
        this.following = false;
    }

    ngOnInit() {
        console.log('Perfil cargado correctamente');
        this.loadPage();
    }

    loadPage() {
        //Obtenemos el parametro 'id' de la url
        this._route.params.subscribe(params => {
            let id = params['id'];
            //Le pasamos este id al metodo getUser
            this.getUser(id);
            this.getCounters(id);
        })
    }

    getUser(id) {
        this._userService.getUser(id).subscribe(
            response => {
                if (response.user) {
                    console.log(response);
                    this.user = response.user;
                    //Si en la response llega que lo estamos siguiendo, establecemos following en true
                    if (response.following && response.following._id) {
                        this.following = true;
                    } else {
                        //Si no nos sigue following pasa a false
                        this.following = false;
                    }
                    //Si en la response llega que nos estan siguiendo, establecemos followed en true
                    if (response.followed && response.followed._id) {
                        this.followed = true;
                    } else {
                        //Si no nos sigue followed pasa a false
                        this.followed = false;
                    }

                } else {
                    this.status = 'error'
                }
            },
            error => {
                console.log(<any>error);
                //Si hay un error, que redireccion a la pagina de perfil del usuario logeado
                this._router.navigate(['/perfil', this.identity._id])
            }
        )
    }

    getCounters(id) {
        //Obtenemos los contadores del usuario pasado por parametro
        this._userService.getCounters(id).subscribe(
            response => {
                console.log(response);
                //Le pasamos a la variable stats el valor que llega por response
                this.stats = response;
            },
            error => {
                console.log(<any>error);
            }
        )
    }

    followUser(followed) {
        var follow: Follow = {
            _id: '',
            user: this.identity._id,
            followed: followed
        }

        this._followService.addFollow(this.token, follow).subscribe(
            response => {
                this.following = true;
            },
            error => {
                console.log(<any>error);
            }
        )
    }

    unfollowUser(followed) {
        this._followService.deleteFollow(this.token, followed).subscribe(
            response => {
                this.following = false;
            },
            error => {
                console.log(<any>error);
            }
        )
    }

    public followUserOver;

    mouseEnter(user_id) {
        this.followUserOver = user_id;
    }

    mouseLeave() {
        this.followUserOver = 0;
    }
}