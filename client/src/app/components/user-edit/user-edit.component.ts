import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { UploadService } from '../../services/upload.service';
import { GLOBAL } from '../../services/global';

@Component({
    selector: 'user-edit',
    templateUrl: './user-edit.component.html',
    providers: [UserService, UploadService]
})

export class UserEditComponent implements OnInit {
    public title: string;
    public user;
    public identity;
    public token;
    public status: string;
    public url: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _uploadService: UploadService
    ) {
        this.title = 'Actualizar mis datos';
        this.user = this._userService.getIdentity();//Con esto obtenemos los datos del localStorage
        this.identity = this.user;
        this.token = this._userService.getToken();//Con esto obtenemos el token del localStorage
        this.url = GLOBAL.url;
    }

    ngOnInit() {
        console.log(this.user);
        console.log('Componente user-edit cargado');
    }

    onSubmit() {
        this._userService.updateUser(this.user).subscribe(
            response => {
                if (!response) {
                    this.status = 'error';
                } else {
                    this.status = 'success';
                    localStorage.setItem('identity', JSON.stringify(this.user));
                    this.identity = this.user;

                    //SUBIDA DE AVATAR
                    this._uploadService.makeFileRequest(this.url + 'upload-avatar/' + this.user._id, [], this.filesToUpload, this.token, 'image')//Al final se pasa "image" que es como se guardara en base de datos
                        .then((result: any) => {
                            //Le damos valor a la imagen del objeto user, le pasamos la imagen que viene como resultado de la promesa
                            this.user.image = result.user.image;
                            //Actualizamos el localStorage con la nueva imagen
                            localStorage.setItem('identity', JSON.stringify(this.user));
                        })
                }
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);

                if (errorMessage != null) {
                    this.status = 'error'
                }
            }
        )
    }

    public filesToUpload: Array<File>;
    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
    }
}