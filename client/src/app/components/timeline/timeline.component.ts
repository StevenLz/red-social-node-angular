import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Publication } from '../../models/publication';
import { UserService } from '../../services/user.service';
import { PublicationService } from '../../services/publication.service';
import { GLOBAL } from '../../services/global';

//DECLARAMOS LAS VARIABLES DE JQUERY
declare var jQuery:any;
declare var $:any;

@Component({
    selector: 'timeline',
    templateUrl: './timeline.component.html',
    providers: [UserService, PublicationService]
})

export class TimelineComponent implements OnInit {
    public title: string;
    public loading: boolean;// Propiedad para activar o desactivar el loader(imagen de carga)
    public identity;
    public token;
    public url: string;
    public status: string;
    public page;
    public total;
    public pages;
    public itemsPerPage;
    public publications: Publication[];

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _publicationService: PublicationService
    ) {
        this.title = 'Timeline';
        this.loading = true; //mantener el loader activo
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.url = GLOBAL.url;
        this.page = 1;
    }

    ngOnInit() {
        console.log('Cargado el componente timeline');
        this.getPublications(this.page);
    }

    getPublications(page, adding = false) {
        this._publicationService.getPublications(this.token, page).subscribe(
            response => {
                if (response.publications) {
                    this.total = response.total;
                    this.pages = response.pages;
                    this.itemsPerPage = response.items_per_page;
                    if (!adding) {
                        this.publications = response.publications;
                        if(!this.publications){
                            alert('Error en el servidor');
                        }else{
                            this.loading = false; //desactivar el loader
                        }
                    } else {
                        var arrayA = this.publications; //Le damos el valor del actual array(actual page)
                        var arrayB = response.publications; //Le damos el valor del nuevo array(next page)

                        this.publications = arrayA.concat(arrayB);//Concatenamos ambos arrays para dar apariencia de scroll infinito

                        $("html, body").animate({ scrollTop: $('html').prop("scrollHeight") }, 500);
                    }

                    if (page > this.pages) {
                        //this._router.navigate(['/timeline']);
                    }
                } else {
                    this.status = 'error';
                }

            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if (errorMessage != null) {
                    this.status = 'error';
                }
            }
        );
    }
    public noMore = false;

    viewMore() {
        this.page += 1;

        if (this.page == this.pages) {
            this.noMore = true;
        }

        this.getPublications(this.page, true)
    }
    //El elemento hijo sidebar nos pasa un evento el cual ejecutara esta funcion
    refresh(event = null){
        //Ejecutamos el metodo para obtener las publicaciones.
        this.getPublications(1);
    }

    deletePublication(id){
        this._publicationService.deletePublication(this.token, id).subscribe(
            response =>{
                this.refresh();
            },
            error =>{
                console.log(<any>error);
            }
        )
    }
}