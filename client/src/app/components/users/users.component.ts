import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Follow } from '../../models/follow';
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';
import { GLOBAL } from '../../services/global';

@Component({
    selector: 'users',
    templateUrl: './users.component.html',
    providers: [UserService, FollowService]
})

export class UsersComponent implements OnInit {
    public title: string;
    public url: string;
    public identity;
    public token;
    public page;
    public next_page;
    public prev_page;
    public total;
    public pages;
    public users: User[];
    public follows;//Usuarios que seguimos
    public status: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _followService: FollowService
    ) {
        this.title = "Gente";
        this.url = GLOBAL.url;
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
    }

    ngOnInit() {
        console.log("El componente de usuarios ha sido cargado");
        this.actualPage();
    }

    actualPage() {
        //Obtener un parametro de la url
        this._route.params.subscribe(params => {
            let page = +params['page']; //Indicamos el parametro de la url y le anteponemos el "+" para indicar que es un entero
            this.page = page;

            if (!params['page']) {//Validamos si en la url viene una pagina, si no page toma el valor de 1
                page = 1;
            }
            if (!page) {
                page = 1;
            } else {
                this.next_page = page + 1;
                this.prev_page = page - 1;

                if (this.prev_page <= 0) {
                    this.prev_page = 1;
                }
            }
            //Devolver listado de usuarios
            this.getUsers(page);
        });
    }

    getUsers(page) {
        this._userService.getUsers(page).subscribe(
            response => {
                if (!response.users) {
                    this.status = "error"
                } else {
                    this.total = response.total;
                    this.users = response.users;
                    this.pages = response.pages;
                    this.follows = response.users_following;
                    console.log(this.follows);

                    if (page > this.pages) {
                        this._router.navigate(['/gente', 1]);//Si se ingresa en la url una pagina mayor a la que nos devuelva la API, entonces lo delvolvemos a la pagina 1
                    }
                }
            },
            error => {
                var errorMessage = <any>error;

                if (errorMessage != null) {
                    this.status = "error";
                }
            }
        );
    }
    public followUserOver;
    //Aca en la funcion recibimos el id del usuario al cual se le pasa el mouse por encima
    mouseEnter(user_id) {
        //Y le pasamos ese id a la variable followUserOver para poder usarla dentro de la condicion en el HTML
        this.followUserOver = user_id;
    }

    mouseLeave(user_id) {
        //Aca establecemos followUserOver a 0 ya que el mouse sale del boton y no queremos que el boton dejar de seguir se muestre mas
        this.followUserOver = 0;
    }

    followUser(followed) {
        var follow : Follow = {
            _id: '',
            user: this.token._id,
            followed: followed
        }
        //Forma alternativa para que guarden todos los datos
        //var follow = JSON.parse('{ "_id":"\'\'", "user":"' + this.identity._id + '", "followed":"' + followed + '" }');

        this._followService.addFollow(this.token, follow).subscribe(
            response => {
                if (!response.follow) {
                    this.status = "error";
                } else {
                    this.status = "success";
                    //Pusheamos el usuario a seguir a nuestro array de follows, 
                    //para que reactivamente se actualice el boton de seguido
                    this.follows.push(followed);
                }
            },
            error => {
                var errorMessage = <any>error;

                if (errorMessage != null) {
                    this.status = "error";
                }
            }
        )
    }

    unfollowUser(followed) {
        this._followService.deleteFollow(this.token, followed).subscribe(
            response => {
                //Buscamos dentro del array de follows el id que traemos por parametro llamado followed
                var search = this.follows.indexOf(followed);

                if (search != -1) {
                    //En caso de que encuentre ese id dentro del array, lo eliminaremos con splice, (index, numero de elementos a elminar)
                    this.follows.splice(search, 1);
                }
            },
            error => {
                var errorMessage = <any>error;

                if (errorMessage != null) {
                    this.status = "error";
                }
            }
        )
    }
}