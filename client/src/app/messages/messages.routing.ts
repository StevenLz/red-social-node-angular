import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'

//Componentes
import { MainComponent } from './components/main/main.component';
import { AddComponent } from './components/add/add.component';
import { ReceivedComponent } from './components/received/received.component';
import { SendedComponent } from './components/sended/sended.component';
import { UserGuard } from '../services/user.guard'; //CONTROL DE ACCESO A LAS URL

const messagesRoutes: Routes = [
    {
        //Ruta padre
        path: 'mensajes',
        component: MainComponent,
        //Rutas hijas
        children: [ //Children sirve para establecer rutas hijas ejemplo:
            { path: '', redirectTo: 'recibidos', pathMatch: 'full' },//Redireccion base
            { path: 'enviar', component: AddComponent, canActivate: [UserGuard] },//En este caso la url seria: http://localhost:4200/mensajes/enviar
            { path: 'recibidos', component: ReceivedComponent, canActivate: [UserGuard] },//En este caso la url seria: http://localhost:4200/mensajes/recibidos
            { path: 'recibidos/:page', component: ReceivedComponent, canActivate: [UserGuard] },//Recibidos con parametro page, en caso de que el usuario ingrese la pagina
            { path: 'enviados', component: SendedComponent, canActivate: [UserGuard] },//En este caso la url seria: http://localhost:4200/mensajes/enviados
            { path: 'enviados/:page', component: SendedComponent, canActivate: [UserGuard] }//Enviados con parametro page, en caso de que el usuario ingrese la pagina
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(messagesRoutes)//Asi se aplican las url creadas anteriormente
    ],
    exports: [
        RouterModule
    ]
})

export class MessagesRoutingModule { }