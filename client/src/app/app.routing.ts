import { ModuleWithProviders } from '@angular/core'
//Routes: Define un array de rutas, mapeando una URL con un path y un componente
//RouterModule: proporciona los proveedores de servicios y las directivas necesarias para navegar a traves de las vistas.
import { Routes, RouterModule } from '@angular/router'

//COMPONENTES
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FollowingComponent } from './components/following/following.component';
import { FollowedComponent } from './components/followed/followed.component';

// CONTROL DE ACCESO A LAS URL
import { UserGuard } from './services/user.guard';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent }, //Pasamos en path el lugar donde se cargara nuestro componente osea localhost:4200/login
    { path: 'registro', component: RegisterComponent },
    { path: 'mis-datos', component: UserEditComponent, canActivate: [UserGuard] },
    { path: 'gente', component: UsersComponent, canActivate: [UserGuard] }, //URL de gente sin parametro, en caso de que el usuario no lo ingrese
    { path: 'gente/:page', component: UsersComponent, canActivate: [UserGuard]  }, //URL de gente con parametro, en caso de que el usuario lo ingrese
    { path: 'timeline', component: TimelineComponent, canActivate: [UserGuard]  },
    { path: 'perfil/:id', component: ProfileComponent, canActivate: [UserGuard]  },
    { path: 'siguiendo/:id/:page', component: FollowingComponent, canActivate: [UserGuard]  },
    { path: 'seguidores/:id/:page', component: FollowedComponent, canActivate: [UserGuard]  },
    { path: '**', component: HomeComponent }//Ruta 404
];

export const appRoutingProviders: any[] = [];
//forRoot: Crea un módulo con todos los proveedores y directivas de enrutadores.
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);