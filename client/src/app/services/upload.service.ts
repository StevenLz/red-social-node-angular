import { Injectable } from '@angular/core';
import { GLOBAL } from './global';

@Injectable()

export class UploadService {
    public url: string;

    constructor() {
        this.url = GLOBAL.url;
    }
    //Hacer una peticion mediante AJAX
    makeFileRequest(url: string, params: Array<string>, files: Array<File>, token: string, name: string) {
        return new Promise(function (resolve, reject) {
            //Creamos una variable formData que sirve para construir un formulario y agregarle los datos que necesitemos
            var formData = new FormData();
            //Creamos la instancia XMLHttpRequest para poder usarlo
            var xhr = new XMLHttpRequest();
            //Iteramos la cantidad de archivos que nos llegan
            for (var i = 0; i < files.length; i++) {
                //Por cada archivo que nos llega, lo agregamos al objeto formdata con el siguiente formato: "formData.append(name, value, filename);"
                formData.append(name, files[i], files[i].name);
            }
            //"onreadystatechange" contiene el manejador del evento que es invocado cuando se dispara el evento readystatechange
            //Lo cual sucede cada vez que cambia el valor de la propiedad readyState 
            xhr.onreadystatechange = function(){
                //readyState devuelve el estado en el que se encuentra la XMLHttpRequest, los estados son:
                //0 : UNSENT - 1 : OPENED - 2 : HEADERS_RECEIVED - 3 : LOADING - 4 - DONE
                if(xhr.readyState == 4){
                    //"status" es el estado en que se ejecuto la request, puede ser 0(fallido) o 200(exitoso)
                    if(xhr.status == 200){
                        //"response" devuelve el contenido del cuerpo
                        resolve(JSON.parse(xhr.response));
                    }else{
                        reject(xhr.response);
                    }
                }
            }
            //"open" inicializa una solicitud recien creada, formato: "XMLHttpRequest.open(method, url, async)""
            xhr.open('POST', url, true);
            //"setRequestHeader" sirve para establecer un valor  de una cabecera HTTP
            xhr.setRequestHeader('Authorization', token);
            //"send" envia la solicitud al servidor
            xhr.send(formData);
        })
    }
}