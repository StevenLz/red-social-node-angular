import { Injectable } from '@angular/core'; //Injectable nos permite definir los servicios y luego inyectarlos en otra clase
import { HttpClient, HttpHeaders} from '@angular/common/http'; //HttpClient: sirve para hacer las peticiones ajax - HttpHeaders: para poder enviar cabeceras en las peticiones ajax
import { Observable } from 'rxjs/Observable'; //Observable: Para poder recoger las respuestas del API - Se debe instalar este modulo con 'npm install --save rxjs-compat' 
import { GLOBAL } from './global'; //Parametros globales de url, etc
import { User } from '../models/user';

@Injectable()

export class UserService{
    public url: string;
    public identity;
    public token;
    public stats;


    constructor(public _http: HttpClient){
        //Traemos la URL global y la pasamos a la variable url
        this.url = GLOBAL.url;
    }
    //Se pone <any> para darle mas libertad a los datos y en caso de que lleguen datos que no sean los podamos recibir
    register(user: User): Observable<any>{
        let params = JSON.stringify(user); //Lo parametros los convertimos en un JSON en string
        let headers = new HttpHeaders().set('Content-Type', 'application/json'); //Pasamos el header de la aplicacion

        return this._http.post(this.url+'register', params, { headers: headers });//Establecemos la peticion por post, traemos la url, luego el service 'register'(donde tenemos el registro del backend), pasamos los parametros y header establecidos anteriormente
    }

    signup(user, gettoken = null): Observable<any>{
        if(gettoken != null){
            user.gettoken = gettoken;
        }

        let params = JSON.stringify(user);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(this.url+'login', params, { headers:headers })
    }

    getIdentity(){
        let identity = JSON.parse(localStorage.getItem('identity'));

        if(identity != "undefined"){
            this.identity = identity;
        }else{
            this.identity = null;
        }

        return this.identity;
    }

    getToken(){
        let token = localStorage.getItem('token');

        if(token != "undefined"){
            this.token = token;
        }else{
            this.token = null;
        }

        return this.token;
    }

    getStats(){
        let stats = JSON.parse(localStorage.getItem('stats'));

        if(stats != 'undefined'){
            this.stats = stats;
        }else{
            this.stats = null;
        }

        return this.stats;
    }

    getCounters(userId = null): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.getToken());

        if(userId != null){
            return this._http.get(this.url+'counters/'+userId, { headers:headers })
        }else{
            return this._http.get(this.url+'counters/', { headers:headers }) 
        }
    }

    updateUser(user): Observable<any>{
        let params = JSON.stringify(user);
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.getToken());
        
        return this._http.put(this.url+'update-user/'+user._id, params, { headers:headers })
    }

    getUsers(page = null): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.getToken());
    
        return this._http.get(this.url+'users/'+page, { headers:headers });
    }

    getUser(id): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', this.getToken());

        return this._http.get(this.url+'user/'+id, { headers:headers })
    }
}