import { Injectable } from '@angular/core'; //Injectable nos permite definir los servicios y luego inyectarlos en otra clase
import { HttpClient, HttpHeaders } from '@angular/common/http'; //HttpClient: sirve para hacer las peticiones ajax - HttpHeaders: para poder enviar cabeceras en las peticiones ajax
import { Observable } from 'rxjs/Observable'; //Observable: Para poder recoger las respuestas del API - Se debe instalar este modulo con 'npm install --save rxjs-compat' 
import { GLOBAL } from './global'; //Parametros globales de url, etc
import { User } from '../models/user';
import { Follow } from '../models/follow';

@Injectable()

export class FollowService {
    public url: string;

    constructor(private _http: HttpClient) {
        this.url = GLOBAL.url;
    }
    //Le pasamos como parametro el objeto follow, es que el que guardaremos en la base de datos
    addFollow(token, follow): Observable<any> {
        let params = JSON.stringify(follow);
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        return this._http.post(this.url + 'follow', params, { headers: headers });
    }
    //Le pasamos el parametro id que es el que usaremos para buscar en la BD y eliminar
    deleteFollow(token, id): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        return this._http.delete(this.url + 'unfollow/' + id, { headers: headers });
    }

    getFollowing(token, userId = null, page = 1): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        //Si no nos llegan parametros, hacemos un llamado a la url sin parametros
        var url = this.url + 'following';
        if (userId != null) {
            //Pero si nos llegan parametros hacemos el llamado a la url con parametros
            url = this.url + 'following/' + userId + '/' + page;
        }
        return this._http.get(url, { headers: headers })
    }

    getFollowed(token, userId = null, page = 1): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        //Si no nos llegan parametros, hacemos un llamado a la url sin parametros
        var url = this.url + 'followed';
        if (userId != null) {
            //Pero si nos llegan parametros hacemos el llamado a la url con parametros
            url = this.url + 'followed/' + userId + '/' + page;
        }

        return this._http.get(url, { headers: headers })
    }

    getMyFollows(token): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        return this._http.get(this.url + 'get-follows/true', { headers: headers });
    }
}
