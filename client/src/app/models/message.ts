export class Message {
    _id: string;
    text: string;
    viewed: string;
    created_at: string;
    emitter: string;
    receiver: string;
    constructor(
        _id: string,
        text: string,
        viewed: string,
        created_at: string,
        emitter: string,
        receiver: string
    ) { }
}