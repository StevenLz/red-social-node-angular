export class User {
    constructor(
        _id: string,
        name: string,
        surname: string,
        nick: string,
        email: string,
        password: string,
        role: string,
        image: string
    ){}
}