export class Publication {
    _id: string;
    text: string;
    file: string;
    created_at: string;
    user: string;

    constructor(
        _id: string,
        text: string,
        file: string,
        created_at: string,
        user: string
    ) {

    }
}