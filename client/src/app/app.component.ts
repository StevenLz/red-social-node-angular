import { Component, OnInit, DoCheck } from '@angular/core'; //El OnInit es el metodo que se ejecuta apenas cargamos un componente
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from './services/user.service';
import { GLOBAL } from './services/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService]
})
export class AppComponent implements OnInit, DoCheck{
  public title:string;
  public identity;
  public url: string;
  public stats;

  constructor(
    private _route:ActivatedRoute,
    private _router:Router,
    private _userService:UserService
  ){
    this.title = 'NODESocial';
    this.url = GLOBAL.url;
  }

  ngOnInit(){
    this.identity = this._userService.getIdentity();
  }
  //Este metodo sirve para que cuando haya un cambio en un componente se refresque
  ngDoCheck(){
    //Se usa para cuando el usuario se loguea tomar la identidad y refrescar el app
    this.identity = this._userService.getIdentity(); //En este caso cada que haya un cambio en getIdentity, el componente se refrescara
    this.stats = this._userService.getStats();
  }

  logout(){
    localStorage.clear();
    this.identity = null;
    this._router.navigate(['/']);
  }
}
